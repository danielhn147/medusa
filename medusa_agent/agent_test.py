#!/usr/bin/python3
import sys, subprocess
import requests
import time
import json

SUPPORTED_COMMANDS = {
    'LED_SINGLE': lambda v: print(v),
    'LED_ALL': lambda p: print(p),
    'LED_TEXT': lambda s: print(s)}


def main():
    while True:
        # request new commands to execute every 2 seconds
        commands_response = requests.get('http://127.0.0.1:8000/api/commands/')
        if commands_response.status_code == 404:
            abort()
        if commands_response.text == None:
            print("Error: Nothing recieved")
        else:
            try:
                new_commands = json.loads(commands_response.text)
            except:
                new_commands = dict()
                print(commands_response.text)
        # execute the commands
        for c, v in new_commands.items():
            SUPPORTED_COMMANDS.get(c, list)(v)

        post_state()

        time.sleep(2)


def abort():
    print('Error: Abort triggered')


def post_state():
    requests.post('http://127.0.0.1:8000/api/state/', data={'humidity': 5.01, 'temperature': 52.02,})


if __name__ == '__main__':
    main()
