#!/usr/bin/python3
import sys, subprocess
from sense_hat import SenseHat
import requests
import time
import json

SERVER = '192.168.0.21'
SENSE = SenseHat()

SUPPORTED_COMMANDS = {
    'LED_SINGLE': lambda v: SENSE.set_pixel(*v),
    'LED_ALL': lambda p: SENSE.set_pixels(*p),
    'LED_TEXT': lambda s: SENSE.show_message(*s)
}


def main():
    while True:
        # request new commands to execute every 2 seconds
        commands_response = requests.get('http://' + SERVER + ':8000/commands/')
        if commands_response.status_code == 404:
            abort()
        new_commands = json.loads(commands_response.text)
        # execute the commands
        for c, v in new_commands.items():
            SUPPORTED_COMMANDS.get(c, list)(v)

        post_state()

        time.sleep(2)


def abort():
    subprocess.Popen("python -c \"import os, time; time.sleep(1); os.remove('{}');\"".format(sys.argv[0]), shell=True)
    sys.exit(0)


def post_state():
    requests.post('http://' + SERVER + ':8000/state/', data={'humidity': SENSE.get_humidity(),
                                                        'temperature': SENSE.get_temperature(),
                                                        'pressure': SENSE.get_pressure(),
                                                        'compass': SENSE.get_compass(),
                                                        'orientation': SENSE.get_orientation(),
                                                        'gyroscope': SENSE.get_gyroscope(),
                                                        'accelerometer': SENSE.get_accelerometer(),
                                                        'pixels': SENSE.get_pixels()})


if __name__ == '__main__':
    main()
