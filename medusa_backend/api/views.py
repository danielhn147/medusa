from django.shortcuts import render
from django.http import HttpResponse
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt
from api.models import StateModel
from django.views.generic import ListView
import json


class StateModelListView(ListView):
    model = StateModel
    template_name = 'saved_states.html'


@csrf_exempt
def commands(request):
    if request.method == 'GET':
        # Clean cache and return current commands
        current_commands = cache.get('commands', '{}')
        cache.set('commands', '{}')
        return HttpResponse(current_commands)

    # Replace all existing commands with their new values, or create new commands
    try:
        new_commands = json.loads(request.body)
        current_commands = cache.get('commands', '{}')
        current_commands = json.loads(current_commands)
        for k in new_commands.keys():
            current_commands[k] = new_commands[k]
        cache.set('commands', json.dumps(current_commands))
    except Exception as e:
        return HttpResponse("Error: " + str(e), 400)
    return HttpResponse("New commands set successfully.")


@csrf_exempt
def state(request):
    if request.method == 'GET':
        # Return the state currently in cache
        return HttpResponse(cache.get('state', '{}'))

    # Set the state
    try:
        cache.set('state', request.body)
    except Exception as e:
        return HttpResponse("Error: " + str(e), 400)
    return HttpResponse("New state set successfully.")


@csrf_exempt
def persist_state(request):
    if request.method == 'GET':
        return HttpResponse("persist_state does not support GET", 404)

    try:
        state_json = cache.get('state', '{}')
        state_dict = json.loads(state_json)
        model = StateModel()
        model.humidity = state_dict.get('humidity', 0.0)
        model.temperature = state_dict.get('temperature', 0.0)
        model.pressure = state_dict.get('pressure', 0.0)
        model.compass = state_dict.get('compass', 0.0)
        model.orientation = state_dict.get('orientation', '{}')
        model.gyroscope = state_dict.get('gyroscope', '{}')
        model.accelerometer = state_dict.get('accelerometer', '{}')
        model.pixels = state_dict.get('pixels', '[]')
        model.save()
    except Exception as e:
        return HttpResponse("Error: " + str(e), 400)
    return HttpResponse("State saved to database successfully.")
