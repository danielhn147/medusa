from django.db import models


class StateModel(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now=True)
    humidity = models.DecimalField(default=0.0, max_digits=19, decimal_places=10)
    temperature = models.DecimalField(default=0.0, max_digits=19, decimal_places=10)
    pressure = models.DecimalField(default=0.0, max_digits=19, decimal_places=10)
    compass = models.DecimalField(default=0.0, max_digits=19, decimal_places=10)
    orientation = models.CharField(max_length=100, default='{}')
    gyroscope = models.CharField(max_length=100, default='{}')
    accelerometer = models.CharField(max_length=100, default='{}')
    pixels = models.CharField(max_length=1300, default='[]')

    def __str__(self):
        return f'{self.id} - {self.date}'
