from django.urls import path
from . import views

urlpatterns = [
    path('commands/', views.commands, name='commands'),
    path('state/', views.state, name='state'),
    path('persist_state/', views.persist_state, name='persist_state'),
    path('saved_states/', views.StateModelListView.as_view(), name='saved_states')
]
